( function() {

    var solution = 0;

    for ( let i = 0; i < 1000; i++ ) {
        if ( !( ( i % 3 ) && ( i % 5 ) ) ) {
            solution += i;
        }

    }

    document.getElementById( 'solution' )
        .innerHTML += ' ' + solution;
} )();
